import { useReducer, createContext } from "react";
// ** Context
export const StoreContext = createContext();
// ** Reducers
import { Reducer } from "./reducer";

export const StoreProvider = ({ children }) => {
	const initialState = {
		data: [],
		profile: [],
		type: "home",
		user: {},
		query: "Car",
	};

	const [state, dispatch] = useReducer(Reducer, initialState);

	return (
		<StoreContext.Provider value={{ ...state, dispatch }}>
			{children}
		</StoreContext.Provider>
	);
};
