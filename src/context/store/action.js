import { fetchApi, generateQueryParams, API_ENDPOINT } from "../../utils/api";

export const fetchImages = async (params = {}, dispatch) => {
	const resp = await fetchApi(
		"get",
		API_ENDPOINT,
		"/search/photos",
		generateQueryParams(params)
	);
	dispatch({ type: "SET", payload: { key: "data", data: resp.results } });
	return resp.results;
};

export const fetchUserImages = async (params = {}, dispatch) => {
	const resp = await fetchApi(
		"get",
		API_ENDPOINT,
		`/users/${params.username}/photos`,
		generateQueryParams(params)
	);
	dispatch({ type: "SET", payload: { key: "profile", data: resp } });
	return resp;
};
