export const Reducer = (prevState, action) => {
	switch (action.type) {
		case "SET":
			return {
				...prevState,
				[action.payload.key]: action.payload.data,
			};
		default:
			return prevState;
	}
};
