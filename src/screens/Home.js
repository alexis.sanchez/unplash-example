// ** hooks
import { useSearch } from "../hooks/search";
import { useStore } from "../hooks/useStore";
// ** Components
import Masonry from "../components/ui-comun/Masonry";
import Header from "../components/ui-home/Header";

const Home = () => {
	const { query } = useStore();
	const objParams = {
		query,
		page: 1,
		per_page: 30,
	};
	const { data } = useSearch(objParams);
	return (
		<>
			<Header />
			<Masonry data={data} type={"home"} />
		</>
	);
};

export default Home;
