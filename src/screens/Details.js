// ** Components
import PagerViews from "../components/ui-details/PageViews";

const Details = ({ route }) => {
	return <PagerViews active={route?.params?.active || 0} />;
};

export default Details;
