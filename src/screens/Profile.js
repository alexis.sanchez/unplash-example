// ** hooks
import { useUserPhotos } from "../hooks/search";
import { useStore } from "../hooks/useStore";
// ** Components
import Masonry from "../components/ui-comun/Masonry";
import Header from "../components/ui-profile/Header";
import ProfileComponent from "../components/ui-profile/ProfileComponent";

const Profile = () => {
	const { user } = useStore();
	const objParams = {
		username: user?.username,
		page: 1,
		per_page: 30,
	};
	const { data } = useUserPhotos(objParams);
	return (
		<>
			<Header />
			<ProfileComponent />
			<Masonry data={data} type={"profile"} />
		</>
	);
};

export default Profile;
