import { useQuery } from "react-query";
import { fetchImages, fetchUserImages } from "../context/store/action";
import { useStore } from "../hooks/useStore";

export const useSearch = (params) => {
	const { dispatch } = useStore();
	return useQuery(`${JSON.stringify(params)}`, () =>
		fetchImages(params, dispatch)
	);
};

export const useUserPhotos = (params) => {
	const { dispatch } = useStore();
	return useQuery(`${JSON.stringify(params)}`, () =>
		fetchUserImages(params, dispatch)
	);
};
