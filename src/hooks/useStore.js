import React from "react";
import { StoreContext } from "../context/store";

export const useStore = () => {
	const context = React.useContext(StoreContext);

	if (context === undefined) {
		throw new Error("StoreContext must be within StoreProvider");
	}

	return context;
};
