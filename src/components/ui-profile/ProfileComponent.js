import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";
// ** hooks
import { useStore } from "../../hooks/useStore";

const ProfileComponent = () => {
	const { user } = useStore();
	return (
		<Main>
			<ContainerUser>
				<RowAvatar>
					<Avatar
						source={{ uri: user?.profile_image?.large }}
						resizeMode="cover"
					/>
				</RowAvatar>
				<RowInfo>
					<NameUser>{user?.name}</NameUser>
					<ViewProfile numberOfLines={3}>{user?.bio}</ViewProfile>
				</RowInfo>
			</ContainerUser>
			<MyPhotos>{"My Photos"}</MyPhotos>
		</Main>
	);
};

export default ProfileComponent;

const MyPhotos = styled.Text`
	font-family: "Bold";
	font-size: ${moderateScale(42, 0.4)}px;
	color: #000000;
`;
const NameUser = styled.Text`
	font-family: "SemiBold";
	font-size: ${moderateScale(22, 0.4)}px;
	margin-bottom: ${moderateScale(5, 0.4)}px;
	color: #000000;
`;
const ViewProfile = styled.Text`
	font-family: "Regular";
	font-size: ${moderateScale(12, 0.4)}px;
	color: #000000;
`;
const Avatar = styled.Image`
	width: ${moderateScale(63, 0.4)}px;
	height: ${moderateScale(63, 0.4)}px;
	border-radius: 50px;
	overflow: hidden;
`;
const RowAvatar = styled.View`
	flex: 1;
`;
const RowInfo = styled.View`
	flex: 4;
	flex-direction: column;
	justify-content: flex-start;
	height: 100%;
`;
const ContainerUser = styled.View`
	flex-direction: row;
	align-items: flex-start;
	width: 100%;
	height: ${moderateScale(80, 0.4)}px;
`;

const Main = styled.View`
	flex-direction: column;
	align-items: flex-start;
	width: 100%;
	padding-horizontal: ${moderateScale(20, 0.4)}px;
	margin-top: ${moderateScale(20, 0.4)}px;
`;
