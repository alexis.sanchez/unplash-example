import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";
import { useNavigation } from "@react-navigation/native";
// ** Components
import IClose from "../../../assets/img/iclose_black.png";

import { Routes } from "../../navigation/routes";

const Header = () => {
	const navigation = useNavigation();
	return (
		<Container>
			<Press onPress={() => navigation.navigate(Routes.HOME)}>
				<Icon source={IClose} resizeMode="contain" />
			</Press>
		</Container>
	);
};

export default Header;

const Press = styled.TouchableOpacity`
	position: absolute;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	left: 25px;
	bottom: 0;
	height: ${moderateScale(37, 0.4)}px;
	width: ${moderateScale(37, 0.4)}px;
	/* border: 1px solid red; */
`;
const Icon = styled.Image`
	width: 100%;
	height: 100%;
`;
const Container = styled.View`
	flex-direction: row;
	justify-content: center;
	padding-top: ${moderateScale(55, 0.6)}px;
	/* border: 1px solid red; */
`;
