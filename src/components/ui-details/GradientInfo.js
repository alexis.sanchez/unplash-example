import { useRef, useEffect } from "react";
import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import { Routes } from "../../navigation/routes";
import { Text, View, Image } from "react-native-animatable";
// ** Hooks
import { useStore } from "../../hooks/useStore";

const GradientInfo = ({ item, visibleInfo, isChange }) => {
	const navigation = useNavigation();
	const { dispatch } = useStore();
	const refInfo = useRef(null);
	const refTitle = useRef(null);
	const refLikes = useRef(null);
	const refAvatar = useRef(null);
	const handleProfile = () => {
		dispatch({ type: "SET", payload: { key: "user", data: item.user } });
		navigation.navigate(Routes.PROFILE);
	};

	// Animations custom
	const slideIn = {
		0: {
			opacity: 0,
			translateX: 500,
		},
		0.3: {
			opacity: 0.3,
			translateX: 400,
		},
		0.5: {
			opacity: 0.5,
			translateX: 300,
		},
		0.7: {
			opacity: 0.7,
			translateX: 100,
		},
		1: {
			opacity: 1,
			translateX: 0,
		},
	};

	const out = {
		1: {
			opacity: 0,
		},
	};

	useEffect(() => {
		if (!!visibleInfo) {
			refInfo.current.animate(slideIn);
			refTitle.current.animate("fadeInUp");
			refLikes.current.animate("fadeInUp");
			refAvatar.current.animate("bounceInLeft");
		} else {
			refInfo.current.animate("slideOutDown");
			refTitle.current.animate("slideOutDown");
			refLikes.current.animate("slideOutDown");
			refAvatar.current.animate("slideOutDown");
		}
	}, [isChange, visibleInfo]);

	return (
		<Gradient
			colors={["rgba(0,0,0,0.2)", "rgba(0,0,0,0.7)"]}
			style={{ transform: [{ translateY: visibleInfo ? 0 : 700 }] }}
		>
			<Title ref={refTitle} numberOfLines={2}>
				{item?.description}
			</Title>
			<Likes ref={refLikes}>{`${item?.likes} Likes`}</Likes>
			<ContainerUser activeOpacity={0.7} onPress={handleProfile}>
				<RowAvatar>
					<Avatar
						ref={refAvatar}
						source={{ uri: item.user.profile_image.medium }}
						resizeMode="cover"
					/>
				</RowAvatar>
				<RowInfo ref={refInfo}>
					<NameUser>{item.user.name}</NameUser>
					<ViewProfile>{"View profile"}</ViewProfile>
				</RowInfo>
			</ContainerUser>
		</Gradient>
	);
};

export default GradientInfo;

const NameUser = styled.Text`
	font-family: "Medium";
	font-size: ${moderateScale(13, 0.4)}px;
	color: #ffffff;
`;
const ViewProfile = styled.Text`
	font-family: "Regular";
	font-size: ${moderateScale(10, 0.4)}px;
	color: #ffffff;
`;
const Avatar = styled(Image)`
	width: ${moderateScale(45, 0.4)}px;
	height: ${moderateScale(45, 0.4)}px;
	border-radius: 50px;
	overflow: hidden;
`;
const RowAvatar = styled.View`
	flex: 1;
`;
const RowInfo = styled(View)`
	flex: 4;
	opacity: 0;
	flex-direction: column;
	justify-content: space-evenly;
	height: 100%;
`;
const ContainerUser = styled.TouchableOpacity`
	flex-direction: row;
	align-items: center;
	height: ${moderateScale(50, 0.6)}px;
	width: 100%;
	/* 	border: 1px solid red; */
`;
const Likes = styled(Text)`
	font-family: "Regular";
	opacity: 0;
	font-size: ${moderateScale(14, 0.4)}px;
	color: #ffffff;
`;
const Title = styled(Text)`
	font-family: "Medium";
	opacity: 0;
	font-size: ${moderateScale(42, 0.4)}px;
	color: #ffffff;
`;
const Gradient = styled(LinearGradient)`
	z-index: 1;
	flex-direction: column;
	justify-content: space-evenly;
	padding: 5px 20px;
	height: ${moderateScale(270, 0.6)}px;
	width: 100%;
	/* border: 1px solid red; */
`;
