import { useRef, useEffect, useState } from "react";
import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";
import PagerView from "react-native-pager-view";
import { useNavigation } from "@react-navigation/native";
// ** hooks
import { useStore } from "../../hooks/useStore";
// ** Components
import GradientInfo from "./GradientInfo";
import IClose from "../../../assets/img/iclose_white.png";
import { Routes } from "../../navigation/routes";

const PagerViews = ({ active }) => {
	const navigation = useNavigation();
	const { data, profile, type } = useStore();
	const [visibleInfo, setVisibleInfo] = useState(false);
	const [isChange, setIsChange] = useState(active);
	const ref = useRef(null);

	const dataRender = type === "home" ? data : profile;

	useEffect(() => {
		ref !== null && ref.current.setPageWithoutAnimation(active);
	}, [ref, active]);

	const toggleInfo = () => {
		setVisibleInfo(!visibleInfo);
	};

	const handleChange = (e) => {
		console.log(e.nativeEvent.position);
		setIsChange(e.nativeEvent.position);
	};

	return (
		<Container>
			<PageWrapper ref={ref} onPageSelected={handleChange}>
				{dataRender?.map((item, index) => (
					<ViewCard key={index} activeOpacity={0.9} onPress={toggleInfo}>
						<Press
							onPress={() =>
								navigation.navigate(
									type === "home" ? Routes.HOME : Routes.PROFILE
								)
							}
						>
							<Icon source={IClose} resizeMode="contain" />
						</Press>
						<BackgroundImage
							source={{ uri: item.urls.regular }}
							resizeMode={"cover"}
						/>
						{index === isChange && (
							<GradientInfo
								item={item}
								visibleInfo={visibleInfo}
								isChange={isChange}
							/>
						)}
					</ViewCard>
				))}
			</PageWrapper>
		</Container>
	);
};

export default PagerViews;

const Press = styled.TouchableOpacity`
	position: absolute;
	z-index: 2;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	left: 25px;
	top: 20px;
	height: ${moderateScale(37, 0.4)}px;
	width: ${moderateScale(37, 0.4)}px;
	/* border: 1px solid red; */
`;
const Icon = styled.Image`
	width: 100%;
	height: 100%;
`;
const Container = styled.View`
	flex: 1;
	background-color: transparent;
`;
const PageWrapper = styled(PagerView)`
	flex: 1;
	background-color: transparent;
	/* border: 1px solid red; */
`;
const ViewCard = styled.TouchableOpacity`
	flex-direction: column;
	justify-content: flex-end;
	height: 100%;
	width: 100%;
	overflow: hidden;
	background-color: #000000;
	/* 	border: 1px solid red; */
`;
const BackgroundImage = styled.Image`
	position: absolute;
	z-index: 0;
	width: 100%;
	height: 100%;
`;
