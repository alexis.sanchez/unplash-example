import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import { Routes } from "../../navigation/routes";
// ** Hooks
import { useStore } from "../../hooks/useStore";

const Card = ({ data, type }) => {
	const navigation = useNavigation();
	const { dispatch } = useStore();
	const { item, index } = data;
	const par = index % 2 === 0;

	const handleNavigate = () => {
		dispatch({ type: "SET", payload: { key: "type", data: type } });
		navigation.navigate(Routes.DETAILS, { active: index });
	};

	return (
		<Container activeOpacity={0.8} par={par} onPress={handleNavigate}>
			<BackgroundImage source={{ uri: item.urls.thumb }} resizeMode={"cover"} />
			<Gradient colors={["rgba(0,0,0,0.0)", "rgba(0,0,0,0.7)"]}>
				<Title numberOfLines={2}>{item?.description}</Title>
				<Likes>{`${item?.likes} Votos`}</Likes>
			</Gradient>
		</Container>
	);
};

export default Card;

const Likes = styled.Text`
	font-family: "Regular";
	font-size: ${moderateScale(10, 0.4)}px;
	color: #ffffff;
`;
const Title = styled.Text`
	font-family: "Medium";
	font-size: ${moderateScale(13, 0.4)}px;
	color: #ffffff;
`;
const Gradient = styled(LinearGradient)`
	z-index: 1;
	flex-direction: column;
	justify-content: space-evenly;
	padding: 15px 10px;
	height: ${moderateScale(70, 0.6)}px;
	width: ${moderateScale(151, 0.6)}px;
	/* border: 1px solid red; */
`;
const Container = styled.TouchableOpacity`
	flex-direction: column;
	justify-content: flex-end;
	border-radius: ${moderateScale(10, 0.6)}px;
	height: ${moderateScale(218, 0.6)}px;
	width: ${moderateScale(151, 0.6)}px;
	margin-top: ${({ par }) => (par ? 10 : 40)}px;
	margin-left: ${({ par }) => (par ? "0px" : "auto")};
	overflow: hidden;
	/* border: 1px solid red; */
`;
const BackgroundImage = styled.Image`
	position: absolute;
	z-index: 0;
	width: 100%;
	height: 100%;
`;
