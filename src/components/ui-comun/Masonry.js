import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";

import Card from "./Card";

const Masonry = ({ data, type }) => {
	const renderItems = (item) => <Card data={item} type={type} />;
	return (
		<MasonryList
			numColumns={2}
			horizontal={false}
			data={data}
			renderItem={renderItems}
			keyExtractor={(_, index) => index.toString()}
			onEndReachedThreshold={0.5}
		/>
	);
};

export default Masonry;

const MasonryList = styled.FlatList`
	z-index: -1;
	margin-top: ${moderateScale(10, 0.6)}px;
	padding-top: ${moderateScale(10, 0.6)}px;
	padding-horizontal: ${moderateScale(20, 0.6)}px;
`;
