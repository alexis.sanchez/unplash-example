import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";
import { Dimensions } from "react-native";
import { useStore } from "../../hooks/useStore";
import IMenu from "../../../assets/img/iclose_white.png";

const windowHeight = Dimensions.get("window").height;
const windowWidth = Dimensions.get("window").width;

const Menu = ({ isOpen, toogleMenu }) => {
	const { dispatch } = useStore();
	const CATEGORIES = [
		"Car",
		"Dog",
		"Cat",
		"Nature",
		"Montains",
		"Linux",
		"MacOS",
	];
	const handleChange = (e) => {
		dispatch({ type: "SET", payload: { key: "query", data: e } });
		toogleMenu();
	};
	const renderItems = ({ item }) => (
		<Item onPress={() => handleChange(item)}>
			<TitleMenu>{item}</TitleMenu>
		</Item>
	);
	return (
		<BackContainer style={{ transform: [{ translateX: isOpen ? 0 : -1000 }] }}>
			<Container>
				<Press onPress={toogleMenu}>
					<Icon source={IMenu} resizeMode="contain" />
				</Press>
				<Title>{`Categorias`}</Title>
				<Categories
					data={CATEGORIES}
					renderItem={renderItems}
					keyExtractor={(_, index) => index.toString()}
				/>
			</Container>
		</BackContainer>
	);
};

export default Menu;

const TitleMenu = styled.Text`
	font-size: ${moderateScale(22, 0.6)}px;
	color: #ffffff;
	font-family: "Bold";
	text-align: center;
`;
const Item = styled.TouchableOpacity`
	flex-direction: row;
	justify-content: center;
	align-items: center;
	height: ${moderateScale(50, 0.4)}px;
	/* border: 1px solid red; */
`;
const Categories = styled.FlatList`
	padding-top: ${moderateScale(20, 0.6)}px;
	padding-horizontal: ${moderateScale(20, 0.6)}px;
`;
const Press = styled.TouchableOpacity`
	position: absolute;
	z-index: 2;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	left: 25px;
	top: 20px;
	height: ${moderateScale(30, 0.4)}px;
	width: ${moderateScale(30, 0.4)}px;
	/* border: 1px solid red; */
`;
const Icon = styled.Image`
	width: 100%;
	height: 100%;
`;
const Container = styled.View`
	position: absolute;
	z-index: 2;
	flex-direction: column;
	padding-top: ${moderateScale(20, 0.4)}px;
	width: ${moderateScale(windowWidth * 0.7, 0.7)}px;
	height: ${windowHeight}px;
	flex-direction: column;
	background-color: #000;
`;
const BackContainer = styled.View`
	position: absolute;
	z-index: 2;
	top: 0;
	left: 0;
	width: ${windowWidth}px;
	height: ${windowHeight}px;
	flex-direction: column;
	background-color: rgba(0, 0, 0, 0.5);
`;
const Title = styled.Text`
	font-size: ${moderateScale(24, 0.6)}px;
	color: #ffffff;
	font-family: "Bold";
	text-align: center;
`;
