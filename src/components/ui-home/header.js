import { useState } from "react";
import styled from "styled-components";
import { moderateScale } from "react-native-size-matters";

// ** Components
import Menu from "./Menu";
import IMenu from "../../../assets/img/imenu.png";

const Header = () => {
	const [isOpen, setIsOpen] = useState(false);

	const toogleMenu = () => {
		setIsOpen(!isOpen);
	};

	return (
		<Container>
			<Press onPress={toogleMenu}>
				<Icon source={IMenu} resizeMode="contain" />
			</Press>
			<Title>{`Discover`}</Title>
			<Menu isOpen={isOpen} toogleMenu={toogleMenu} />
		</Container>
	);
};

export default Header;

const Press = styled.TouchableOpacity`
	position: absolute;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	left: 25px;
	bottom: 0;
	height: ${moderateScale(30, 0.4)}px;
	width: ${moderateScale(30, 0.4)}px;
	/* 	border: 1px solid red; */
`;
const Icon = styled.Image``;
const Container = styled.View`
	flex-direction: row;
	justify-content: center;
	padding-top: ${moderateScale(25, 0.6)}px;
	/* border: 1px solid red; */
`;
const Title = styled.Text`
	font-size: ${moderateScale(24, 0.6)}px;
	font-family: "Bold";
	text-align: center;
`;
