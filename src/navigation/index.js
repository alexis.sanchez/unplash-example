import { StatusBar } from "react-native";
import {
	createStackNavigator,
	CardStyleInterpolators,
} from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { QueryClient, QueryClientProvider } from "react-query";

// ** Screens
import Home from "../screens/Home";
import Details from "../screens/Details";
import Profile from "../screens/Profile";
import { Routes } from "./routes";

// ** Context & Providers
import { StoreProvider } from "../context/store";

const queryClient = new QueryClient();

const RootStack = createStackNavigator();

const openUp = {
	gestureDirection: "vertical",
	cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
};

const RootStackScreen = () => {
	return (
		<RootStack.Navigator screenOptions={{ headerShown: false }}>
			<RootStack.Screen name={Routes.HOME} component={Home} />
			<RootStack.Screen
				name={Routes.DETAILS}
				component={Details}
				options={{ ...openUp }}
			/>
			<RootStack.Screen name={Routes.PROFILE} component={Profile} />
		</RootStack.Navigator>
	);
};

export const RootStackNavigator = () => {
	return (
		<NavigationContainer>
			<StoreProvider>
				<QueryClientProvider client={queryClient}>
					<StatusBar style="light" />
					<RootStackScreen />
				</QueryClientProvider>
			</StoreProvider>
		</NavigationContainer>
	);
};
