const API_KEY =
	"a2f508640cb62f314e0e0763594d40aab1c858a7ef796184067c537a88b276aa";
export const API_ENDPOINT = "https://api.unsplash.com";
const API_ORIENTATION = "portrait";

export const fetchApi = async (method, url, path, queryParams) => {
	const res = await fetch(`${url}${path}?client_id=${API_KEY}${queryParams}`, {
		method,
		headers: {
			Accept: "application/json",
		},
	});
	return res.json();
};

export const generateQueryParams = (params) => {
	let queryParams = "";
	console.log("params", params);
	queryParams = queryParams.concat(`&orientation=${API_ORIENTATION}`);
	if (params?.query) {
		queryParams = queryParams.concat(`&query=${params.query}`);
	}
	if (params?.page) {
		queryParams = queryParams.concat(`&page=${params.page}`);
	}
	if (params?.per_page) {
		queryParams = queryParams.concat(`&per_page=${params.per_page}`);
	}
	if (params?.order_by) {
		queryParams = queryParams.concat(`&order_by=${params.order_by}`);
	}
	queryParams = queryParams.concat("&featured");
	console.log("queryParams", queryParams);
	return queryParams;
};
