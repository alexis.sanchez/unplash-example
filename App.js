import { useState, useEffect } from "react";
import { LogBox } from "react-native";
import * as Font from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import styled from "styled-components";
import { RootStackNavigator } from "./src/navigation";
import { Text, TextInput } from "react-native";

export default function App() {
	LogBox.ignoreAllLogs(true);
	const [appIsReady, setAppIsReady] = useState(false);

	const prepare = async () => {
		try {
			await SplashScreen.preventAutoHideAsync();
			await Font.loadAsync({
				Light: require("./assets/fonts/MuseoSans-100.otf"),
				Regular: require("./assets/fonts/MuseoSans-300.otf"),
				Medium: require("./assets/fonts/MuseoSans_500.otf"),
				SemiBold: require("./assets/fonts/MuseoSans_700.otf"),
				Bold: require("./assets/fonts/MuseoSans_900.otf"),
			});
			await new Promise((resolve) => setTimeout(resolve, 2000));
		} catch (e) {
			console.warn(e);
		} finally {
			setAppIsReady(true);
			SplashScreen.hideAsync();
		}
	};

	useEffect(() => {
		prepare();
	}, []);

	if (!appIsReady) {
		return <Container />;
	}

	return <RootStackNavigator />;
}

const Container = styled.View`
	flex-direction: column;
	flex: 1;
`;

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;
